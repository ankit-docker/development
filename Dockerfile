FROM golang:stretch
LABEL maintainer="Ankit R Gadiya"

ENV LINT_VERSION 1.22.2
ENV BAT_VERSION 0.12.1
ENV PROTOC_VERSION 3.11.2

RUN	apt update \
	&& apt install bash-completion vim ranger git unzip -y \
	&& wget https://github.com/protocolbuffers/protobuf/releases/download/v${PROTOC_VERSION}/protoc-${PROTOC_VERSION}-linux-x86_64.zip \
	&& unzip protoc-${PROTOC_VERSION}-linux-x86_64.zip \
	&& mv bin/protoc /usr/local/bin/protoc \
	&& rm -rf bin/ include/ src/ readme.txt protoc-${PROTOC_VERSION}-linux-x86_64.zip \
	&& wget https://github.com/golangci/golangci-lint/releases/download/v${LINT_VERSION}/golangci-lint-${LINT_VERSION}-linux-amd64.tar.gz \
	&& tar -xvf golangci-lint-${LINT_VERSION}-linux-amd64.tar.gz \
	&& rm golangci-lint-${LINT_VERSION}-linux-amd64.tar.gz \
	&& mv golangci-lint-${LINT_VERSION}-linux-amd64/golangci-lint /usr/local/bin/golangci-lint \
	&& rm -rf golangci-lint-${LINT_VERSION}-linux-amd64 \
	&& wget https://github.com/sharkdp/bat/releases/download/v${BAT_VERSION}/bat_${BAT_VERSION}_amd64.deb \
	&& apt install ./bat_${BAT_VERSION}_amd64.deb \
	&& rm bat_${BAT_VERSION}_amd64.deb \
	&& go get -u -v github.com/golang/mock/mockgen \
	&& go get -u -v golang.org/x/tools/... \
	&& go get -u -v github.com/golang/protobuf/protoc-gen-go \
	&& go get -u -v github.com/go-delve/delve/cmd/dlv

COPY "./entrypoint.sh" "/entrypoint.sh"

ENV GOPROXY "https://proxy.golang.org,direct"
ENV GOPRIVATE "github.com/zaubatechnologies/*"


ENTRYPOINT ["/entrypoint.sh"]
